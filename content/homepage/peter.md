---
title: 'Peter Gluckman'
weight: 5
header_menu: true
---
![Jane Doe](images/PG1.jpg)


Peter is an experienced trouble shooter and problem solver for top teams and organisations, with extensive board and executive experience covering health, local government, charities, and voluntary associations.  His experience covers: 
- Complex systems involving many specialties or agencies that are keen or need to collaborate, but experience frustration or barriers to reaching agreement on how to make progress together
- Bringing a supportive but challenging approach to his work with Boards, executive teams, and groups of all backgrounds by identifying shared aims and reducing tension through negotiation and conflict resolution strategies within and between organisations and sectors.  His aim is to assist others in creating innovative yet practical means by which the collaborating organisations achieve more together than they do separately.

Recently Peter has provided independent chairing and facilitation of multi-agency groups and enterprises confronting difficult decisions and transformation programmes.  He has led service strategies, reviews, evaluation, health and equalities impact assessments and service reconfigurations.  He has mentored a wide range of clinicians and senior managers over the last twenty-five years. These include international research clinicians, NHS Trust executives, senior nurses and senior managers appointed as board-level directors for the first time; and senior managers for whom director-level posts are their next appointment.  His work is focused on enabling them to understand the tensions and multiple objectives impacting on them and to develop techniques to achieve resilience at both personal and professional levels.

His support to public health approaches throughout his NHS career led to the Faculty of Public Health awarding him honorary membership in 2000.
Peter is also the co-founder and director of  the PHAST, the Public Health Action Support Team (www.phast.org.uk), a community interest social enterprise company, and he is a specialist consultant on patient, service user and public engagement to Verve Communications (www.vervecommunications.co.uk).

Contact: peter.gluckman@change-fx.com

https://www.linkedin.com/in/peter-gluckman-1b799419/
