---
title: 'Prof David Mathew'
weight: 3
header_menu: true
---
<!-- ![Jane Doe](images/GT.jpg) -->

David's main consulting focus is on coaching individuals and senior management teams and advising on leadership development and organizational change. His clients range from global brands, European retail and media companies, to the NHS and UK public sector. He has worked with senior civil servants from most UK government departments.

He is the Founder of industry-leading Teams and Leadership (www.teamsandleadership.com), a cloud-based team and leadership development platform empowering organisational change. The platform offers free resources on team and leadership development for individuals and teams who want to increase their effectiveness and ability to work well together without the help and expense of an external coach.

Before founding Change-fx with Simon Standish in 2000, David worked at top management level for The Body Shop International plc and as Deputy Director at The King's Fund, the UK's leading healthcare think-tank. His early career was spent in management consulting in European multi-nationals, and at the BBC where he spent ten years as a senior manager, first in market research and then in television production.

He has held Visiting Professor roles in The Business Schools at Imperial College, London and Kingston University. He spends his research time reviewing the latest high quality evidence on organisation development (mainly meta-analyses) then spends a lot more time on making the results accessible to everyone.

Contact: david.mathew@teamsandleadership.com

https://www.linkedin.com/in/davidmathew/



