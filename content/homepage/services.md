---
title: 'Simon Standish'
weight: 4
header_menu: true
---
![Simon Standish](images/Simon4.jpg)

Simon is a specialist organisational change and development consultant working in both the private and public sectors. He is experienced as a project manager, process consultant and team facilitator in situations varying from organisational mergers, to the design and start-up of new services and organizations. He has particular skills in assessing and developing strategies for change as well as helping clients on implementation through the application of sound programme and project management. Group and team facilitation are a key part of his work with a particular emphasis on Board and top team effectiveness. His passion is for enabling cross functional and cross organisation teams to secure beneficial change with speed.

Simon's change management skills were developing during his early career within operations management and human resources working for Esso and Phillips Petroleum. He extended the range of these skills across sectors working as a management consultant with Price Waterhouse, Kinsley Lord and Ashridge Consulting. He formed Change-fx with David Mathew in 2000.

He knows the world of healthcare very well having worked across private, public and third sector providers of hospital and community based services. Recent projects have included developing new organisational and service strategies for NHS organisations as well as supporting the practical aspects of service integration. His work in public services has extended internationally to include work on the restoration of services in Iraq and EU projects in Eastern Europe on effective governance. Simon has experience as a NHS Non Executive Director. 

He is co-author of the book, People Projects and Change published by Premium Books in 2006.

Contact: simon.standish@change-fx.com

https://www.linkedin.com/in/simon-standish-1061a25/



