---
title: 'Annabel Scarfe MBE'
weight: 2
header_menu: true
---
<!--![Jane Doe](images/happy-ethnic-woman-sitting-at-table-with-laptop-3769021.jpg)-->


Annabel is an experienced director (both Executive and non-Executive), facilitator, consultant, and coach.  She knows health and social care very well having worked with and for the NHS and its partner organisations for over 30 years. 

Annabel is a skilful facilitator and coach who works with boards, executive teams, and senior multi-professional teams and with individuals on a one to one basis. She has expertise in designing and then managing large-scale organisation and leadership development programmes within the context of merger and new start-ups. She moves from concept to detailed delivery with ease and her expertise within leadership and OD is widely recognised. She has designed and led talent management programmes for Chief Executives and Directors and has an outstanding track record in the design and delivery of tailored selection and development centres.

https://www.linkedin.com/in/annabel-scarfe-1a494b9/

